let appConfig = envServiceProvider => {
  'ngInject';

  envServiceProvider.config({
    domains: {
      development: ['localhost'],
      production: ['localhost'],
      test: ['localhost'],
      // anotherStage: ['domain1', 'domain2']
    },
    vars: {
      development: {
        apiUrl: 'https://rh-tasks-scheduler-lifter21.c9users.io',
        // staticUrl: '//static.acme.dev.local',
        // antoherCustomVar: 'ipsum'
      },
      test: {
        // apiUrl: '//api.acme.dev.test/v1',
        // staticUrl: '//static.acme.dev.test',
        // antoherCustomVar: 'ipsum'
      },
      production: {
        // apiUrl: '//api.acme.com/v1',
        // staticUrl: '//static.acme.com',
        // antoherCustomVar: 'ipsum'
      },
      // anotherStage: {
      // 	customVar: 'lorem',
      // 	customVar: 'ipsum'
      // },
      defaults: {
        apiUrl: '//https://rh-tasks-scheduler-lifter21.c9users.io',
        staticUrl: '//localhost:3000'
      }
    }
  });

  envServiceProvider.check();
};

export default appConfig;


