import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import ngMaterial from 'angular-material';
import ngResource from 'angular-resource';
import environment from 'angular-environment';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import appConfig from './app.config';
import 'normalize.css';
import '../../node_modules/angular-material/angular-material.css';

angular.module('app', [
    uiRouter,
    ngMaterial,
    ngResource,
    environment,
    Common,
    Components
  ])
  .config(($locationProvider, $httpProvider) => {
    'ngInject';
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  })
  .config(appConfig)

  .component('app', AppComponent);
