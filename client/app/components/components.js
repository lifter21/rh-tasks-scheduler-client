import angular from 'angular';
import Home from './home/home';
import Job from './job/job';
import Host from './host/host';
import History from './history/history';

let componentModule = angular.module('app.components', [
  Home,
  Job,
  Host,
  History
])

.name;

export default componentModule;
