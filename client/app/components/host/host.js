import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import hostComponent from './host.component';
import HostsFactory from './host.factory';
import HostService from './host.service';

let hostModule = angular.module('host', [
  uiRouter
])

.component('host', hostComponent)
.factory('HostsFactory', HostsFactory)
.service({HostService})

.name;

export default hostModule;
