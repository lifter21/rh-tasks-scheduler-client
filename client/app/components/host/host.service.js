class HostService {
  constructor(HostsFactory) {
    'ngInject';

    this.HostsFactory = HostsFactory;
    this.hosts = [];
  }

  getAll() {
    this.HostsFactory.query().$promise.then(res => this.hosts = res.data);
  }

  delete(host) {
    this.HostsFactory.delete({hostId: host._id}).$promise
      .then(() => {
        this.getAll();
      })
      .catch(console.error);
  }

  save(data) {
    let createOrUpdate = data => {
      let host = new this.HostsFactory(data);
      
      return host._id ? host.$update() : host.$save();
    }
    
    return createOrUpdate(data)
      .then(() => {
        this.getAll();
      })
      .catch(console.error);
  }
}

export default HostService;
