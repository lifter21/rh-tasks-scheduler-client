import template from './host.html';
import controller from './host.controller';
import './host.scss';

let hostComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default hostComponent;
