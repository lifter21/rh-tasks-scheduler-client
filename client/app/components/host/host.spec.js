import HostModule from './host';
import HostController from './host.controller';
import HostComponent from './host.component';
import HostTemplate from './host.html';

describe('Host', () => {
  let $rootScope, makeController;

  beforeEach(window.module(HostModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new HostController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(HostTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = HostComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(HostTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(HostController);
    });
  });
});
