HostDialogController.$inject = ['$mdDialog', 'host', 'HostService'];

function HostDialogController ($mdDialog, host, HostService) {
  'use strict';
  
  let me = this;

  me.HostService = HostService;

  me.host = host ? angular.copy(host) : {};
  me.title = host._id ? 'Edit the Host' : 'Create a Host';

  me.hide = () => {
    $mdDialog.hide();
  };

  me.cancel = () => {
    $mdDialog.cancel();
  };

  me.submit = () => {
    $mdDialog.hide(me.host);
  };
  
}

export default HostDialogController;
