'use strict';

let HostsFactory = function ($resource, envService) {
  'ngInject';

  const apiUrl = envService.read('apiUrl');
  const hostsPath = `${apiUrl}/hosts`;
  const hostPath = `${hostsPath}/:hostId`;

  return $resource(hostPath, {hostId: '@_id'}, {
    query: {isArray: false},
    update: {method: 'PUT'}
  });
};

export default HostsFactory
