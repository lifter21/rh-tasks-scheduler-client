JobHistoryDialogController.$inject = ['$mdDialog', 'job', 'historyRecords'];

function JobHistoryDialogController($mdDialog, job, historyRecords) {
  'use strict';
 
  let me = this;

  me.title = `${job.name} job history`;

  me.historyRecords = historyRecords;

  me.hide = () => {
    $mdDialog.hide();
  };

}
 
export default JobHistoryDialogController;