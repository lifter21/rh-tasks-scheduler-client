import JobModule from './job';
import JobController from './job.controller';
import JobComponent from './job.component';
import JobTemplate from './job.html';

describe('Job', () => {
  let $rootScope, makeController;

  beforeEach(window.module(JobModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new JobController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(JobTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = JobComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(JobTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(JobController);
    });
  });
});
