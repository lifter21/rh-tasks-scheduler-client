class JobService {
  constructor(JobsFactory) {
    'ngInject';

    this.JobsFactory = JobsFactory;
    this.jobs = [];
    this.isReady = false;
  }

  getAll() {
    this.JobsFactory.query().$promise
    .then(res => {
      this.jobs = res.data;
      this.isReady = true;
    });
  }

  delete(job) {
    this.JobsFactory.delete({jobId: job._id}).$promise
      .catch(console.error)
      .finally(() => {
        this.getAll();
      });
  }

  save(data) {
    let job = new this.JobsFactory(data);

    saveOrUpdate(job)
      .then(() => {
        this.getAll();
      })
      .catch(console.error);

    function saveOrUpdate(job) {
      return job._id ? job.$update() : job.$save();
    }
  }

  restore(job) {
    this.JobsFactory.restore({jobId: job._id}, {}).$promise
      .then(() => {
        this.getAll();
      })
      .catch(console.error);
  }

  stop(job) {
    this.JobsFactory.stop({jobId: job._id}, {}).$promise
      .then(() => {
        this.getAll();
      })
      .catch(console.error);
  }

  schedule(job) {
    this.JobsFactory.schedule({jobId: job._id}, {}).$promise
      .then(() => {
        this.getAll();
      })
      .catch(console.error);
  }
  
  getHistory(job) {
    return this.JobsFactory
      .getHistory({jobId: job._id}).$promise
      .then(res => res.data);
  }
}

export default JobService;
