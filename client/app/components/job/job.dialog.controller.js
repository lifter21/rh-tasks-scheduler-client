import HostDialogController from '../host/host.dialog.controller'
import template from '../host/host.dialog.html'


JobDialogController.$inject = ['$mdDialog', 'job', 'HostService'];

function JobDialogController ($mdDialog, job, HostService) {
  'use strict';
  
  let me = this;

  me.HostService = HostService;

  me.HostService.getAll();

  me.job = job ? angular.copy(job) : {host: null};
  me.title = job._id ? 'Edit the Job' : 'Create a Job';

  me.hide = () => {
    $mdDialog.hide();
  };

  me.cancel = () => {
    $mdDialog.cancel();
  };

  me.submit = () => {
    $mdDialog.hide(me.job);
  };
  
  me.openHostForm = function (evt, host) {
     $mdDialog.show({
      controller: HostDialogController,
      controllerAs: '$ctrl',
      template: template,
      multiple: true,
      locals: {
        host: host ? host : {}
      },
      // parent: angular.element(document.querySelector('#jobDialogContainer')),
      parent: angular.element(document.body),
      targetEvent: evt,
      clickOutsideToClose: false
    })
      .then(hostData => {
        me.HostService.save(hostData);
      }, function () {
        console.log('You cancelled the dialog.');
      });
  };
}

export default JobDialogController;
