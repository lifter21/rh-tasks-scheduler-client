import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import jobComponent from './job.component';
import JobsFactory from './job.factory';
import JobService from './job.service';

let jobModule = angular.module('job', [
  uiRouter
])

.component('job', jobComponent)
.factory('JobsFactory', JobsFactory)
.service({ JobService })

.name;

export default jobModule;
