'use strict';


let JobsFactory = function ($resource, envService) {
  'ngInject';

  const apiUrl = envService.read('apiUrl');
  const jobsPath = `${apiUrl}/jobs`;
  const jobPath = `${jobsPath}/:jobId`;

  return $resource(jobPath, {jobId: '@_id'}, {
    query: {isArray: false},
    update: {method: 'PUT'},
    getHistory: {method: 'GET', url: `${jobPath}/history`},
    schedule: {method: 'PUT', url: `${jobPath}/schedule`},
    restore: {method: 'PUT', url: `${jobPath}/restore`},
    stop: {method: 'PUT', url: `${jobPath}/stop`},
  });
};

export default JobsFactory
