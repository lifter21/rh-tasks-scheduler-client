import jobDialogTemplate from './job.dialog.html';
import jobHistoryDialogTemplate from './job.history.dialog.html';
import JobDialogController from './job.dialog.controller';
import JobHistoryDialogController from './job.history.dialog.controller';

class JobController {
  constructor($mdDialog, JobService) {
    'ngInject';

    this.$mdDialog = $mdDialog;
    this.JobService = JobService;

    this.name = 'job';

    this.JobService.getAll();
  }

  openJobDialog(evt, job) {
    this.$mdDialog.show({
        controller: JobDialogController,
        controllerAs: '$ctrl',
        template: jobDialogTemplate,
        locals: {
          job: job ? job : {}
        },
        multiple: true,
        parent: angular.element(document.body),
        targetEvent: evt,
        clickOutsideToClose: false
      })
      .then(jobData => {
        this.JobService.save(jobData);
      }, function() {
        console.log('You cancelled the dialog.');
      });
  }

  confirmJobDelete(evt, job) {
    let confirm = this.$mdDialog.confirm()
      .title('Would you like to mark a Job as deleted?')
      .textContent('Confirm to continue')
      .ariaLabel('Lucky day')
      .targetEvent(evt)
      .ok('Delete')
      .cancel('Cancel');

    this.$mdDialog.show(confirm)
      .then(() => {
        this.JobService.delete(job);
      }, () => {});
  }

  openHistoryDialog(evt, job) {
    this.$mdDialog.show({
      controller: JobHistoryDialogController,
      controllerAs: '$ctrl',
      template: jobHistoryDialogTemplate,
      locals: {
        job: job,
        historyRecords: this.JobService.getHistory(job)
      },
      multiple: true,
      parent: angular.element(document.body),
      targetEvent: evt,
      clickOutsideToClose: false
    });
  }

  restoreJob(evt, job) {
    this.JobService.restore(job);
  }
  
  scheduleJob(evt, job) {
    this.JobService.schedule(job);
  }
  
  stopJob(evt, job) {
    this.JobService.stop(job);
  }
}

export default JobController;
