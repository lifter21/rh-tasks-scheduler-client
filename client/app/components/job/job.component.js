import template from './job.html';
import controller from './job.controller';
import './job.scss';

let jobComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default jobComponent;
